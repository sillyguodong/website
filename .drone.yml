---
kind: pipeline
type: docker
name: Build and deploy website PR preview

platform:
  os: linux
  arch: arm64

trigger:
  event:
    - pull_request

steps:
  - name: build website
    pull: always
    image: golang:1.19
    commands:
      - make clean
      - make build

  - name: "[PR] create s3 bucket"
    when:
      event:
        - pull_request
    pull: always
    image: alpine:latest
    environment:
      AWS_ACCESS_KEY_ID:
        from_secret: preview_aws_access_token
      AWS_SECRET_ACCESS_KEY:
        from_secret: preview_aws_secret_access_key
      AWS_REGION: eu-central-1
    commands:
      - apk add --no-cache aws-cli
      - aws s3api create-bucket --acl public-read --bucket preview-gitea-org-website-${DRONE_REPO_NAME}-${DRONE_PULL_REQUEST} --region $${AWS_REGION} --create-bucket-configuration LocationConstraint=$${AWS_REGION} || /bin/true
      - aws s3 website s3://preview-gitea-org-website-${DRONE_REPO_NAME}-${DRONE_PULL_REQUEST}/ --index-document index.html --error-document error.html || /bin/true

  - name: "[PR] deploy website to S3 bucket"
    when:
      event:
        - pull_request
    pull: always
    image: plugins/s3-sync
    environment:
      AWS_ACCESS_KEY_ID:
        from_secret: preview_aws_access_token
      AWS_SECRET_ACCESS_KEY:
        from_secret: preview_aws_secret_access_key
      AWS_REGION: eu-central-1
    settings:
      source: public/
      target: /
      region: eu-central-1
      bucket: preview-gitea-org-website-${DRONE_REPO_NAME}-${DRONE_PULL_REQUEST}
      acl: public-read

  - name: "[PR] Post comment to PR"
    when:
      event:
        - pull_request
    image: byrnedo/alpine-curl
    environment:
      GITEA_TOKEN:
        from_secret: access_token
      AWS_REGION: eu-central-1
    commands:
      # approach: check if comment already exists to prevent spamming in future runs
      - 'COMMENTS=$(curl -sL -X GET -H "Authorization: token $GITEA_TOKEN" https://gitea.com/api/v1/repos/${DRONE_REPO_OWNER}/${DRONE_REPO_NAME}/issues/${DRONE_PULL_REQUEST}/comments)'
      - 'if [[ $COMMENTS == "[]" ]]; then curl -sL -X POST -H "Authorization: token $GITEA_TOKEN" -H "Content-type: application/json" https://gitea.com/api/v1/repos/${DRONE_REPO_OWNER}/${DRONE_REPO_NAME}/issues/${DRONE_PULL_REQUEST}/comments -d "{\"body\": \"Website preview: http://preview-gitea-org-website-${DRONE_REPO_NAME}-${DRONE_PULL_REQUEST}.s3-website.$${AWS_REGION}.amazonaws.com/\"}"; else echo -e "\n INFO: Comment already exist, doing nothing"; fi'

  ### NB: not working as of 2023-02-06 due to Drone ignoring the Gitea webhook for PR close events: https://community.harness.io/t/closing-pull-request/13205
  # - name: "[PR] Delete S3 bucket after closing PR"
  #   image: byrnedo/alpine-curl
  #   environment:
  #     preview_aws_access_token:
  #       from_secret: preview_aws_access_token
  #     preview_aws_secret_access_key:
  #       from_secret: preview_aws_secret_access_key
  #     AWS_REGION: eu-central-1
  #   commands:
  #     - apk add --no-cache jq
  #     # check if PR got closed
  #     - "PR_STATE=$(curl https://gitea.com/api/v1/repos/${DRONE_REPO_OWNER}/${DRONE_REPO_NAME}/pulls/${DRONE_PULL_REQUEST} | jq -r .state)"
  #     # delete S3 if PR is closed
  #     - "if [[ $PR_STATE == 'closed' ]]; then aws s3 rb s3://preview-gitea-org-website-${DRONE_REPO_NAME}-${DRONE_PULL_REQUEST} --force; else echo -e '\n INFO: PR not in state closed, doing nothing'; fi"

volumes:
  - name: cache
    temp: {}
